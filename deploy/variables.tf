variable "region" {
  default = "eu-west-1"
}

variable "project" {
  default = "tg-goals-app"
}

variable "keyname" {
  default = "AWSTomekGunter"
}

variable "db_user" {
  description = "Username for the RDS Postgres instance"
}

variable "db_pass" {
  description = "Password for the RDS postgres instance"
}

variable "db_url" {
  description = "Password for the RDS postgres instance"
}

variable "ecr_backend_image" {
  description = "ECR Image for backend"
  default     = "677561402756.dkr.ecr.eu-west-1.amazonaws.com/tg-goals-app:latest"
}
