data "aws_ami" "amazon_linux" {
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"] //for latest avaible version
  }
  owners = ["amazon"]
}

resource "aws_iam_instance_profile" "ec2" {
  name = "${var.project}-ec2-instance-profile"
  role = aws_iam_role.ec2.name
}

resource "aws_iam_role" "ec2" {
  name               = "${var.project}-ec2"
  assume_role_policy = file("./templates/ec2/instance-profile-policy.json")

  tags = local.common_tags
}

resource "aws_iam_role_policy_attachment" "ec2_attach_policy" {
  role       = aws_iam_role.ec2.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

resource "aws_instance" "ec2" {
  ami                  = data.aws_ami.amazon_linux.id
  instance_type        = "t2.micro"
  user_data            = file("./templates/ec2/user-data.sh")
  subnet_id            = aws_subnet.private_a.id
  private_ip           = "10.1.10.250"
  key_name             = var.keyname
  iam_instance_profile = aws_iam_instance_profile.ec2.name

  vpc_security_group_ids = [
    aws_security_group.ec2.id
  ]

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${var.project}-ec2" })
  )
}

resource "aws_security_group" "ec2" {
  description = "Control ec2 inbound and outbound access"
  name        = "${var.project}-ec2"
  vpc_id      = aws_vpc.main.id

  ingress {
    protocol  = "tcp"
    from_port = 22
    to_port   = 22
    cidr_blocks = [
      aws_subnet.private_a.cidr_block,
      aws_subnet.private_b.cidr_block
    ]
  }

  ingress {
    from_port = 27017
    to_port   = 27017
    protocol  = "tcp"
    cidr_blocks = [
      aws_subnet.private_a.cidr_block,
      aws_subnet.private_b.cidr_block
    ]
  }

  egress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  # egress {
  #   from_port = 27017
  #   to_port   = 27017
  #   protocol  = "tcp"
  #   cidr_blocks = [
  #   aws_subnet.public_a.cidr_block]
  # }

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${var.project}-ec2" })
  )
}
