output "bastion_host" {
  value = aws_instance.bastion.public_dns
}

output "goals_backend_endpoint" {
  value = aws_lb.goals.dns_name
}

