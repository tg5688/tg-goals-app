[
    {
        "name": "goals-backend",
        "image": "${backend_image}",
        "essential": true,
        "memoryReservation": 256,
        "environment": [
            {"name": "MONGODB_PASSWORD", "value": "${db_pass}"},
            {"name": "MONGODB_URL", "value": "${db_url}"},
            {"name": "MONGODB_USERNAME", "value": "${db_user}"}
        ],
        "logConfiguration": {
            "logDriver": "awslogs",
            "options": {
                "awslogs-group": "${log_group_name}",
                "awslogs-region": "${log_group_region}",
                "awslogs-stream-prefix": "ecs"
            }
        },
        "portMappings": [
            {
              "hostPort": 80,
              "protocol": "tcp",
              "containerPort": 80
            }
        ],
        "command": [
          "node",
          "app.js"
        ]
    }
]
