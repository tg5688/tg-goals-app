#!/bin/bash

sudo yum update -y
sudo amazon-linux-extras install -y docker
sudo systemctl enable docker.service
sudo systemctl start docker.service
sudo usermod -aG docker ec2-user

sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# wget https://downloads.mongodb.com/compass/mongosh-1.0.3-linux-x64.tgz
# tar -zxvf mongosh-1.0.3-linux-x64.tgz
# chmod +x mongosh-1.0.3-linux-x64/bin/mongosh
# chmod +x mongosh-1.0.3-linux-x64/bin/mongocryptd-mongosh
# sudo cp mongosh-1.0.3-linux-x64/bin/mongosh /usr/local/bin/
# sudo cp mongosh-1.0.3-linux-x64/bin/mongocryptd-mongosh /usr/local/bin/

mkdir docker
mkdir -p ./docker/env

cat << EOF >> ./docker/docker-compose.yml
version: '3.8'
services:
  mongodb:
    image: 'mongo'
    volumes:
      - data:/data/db
    env_file:
      - ./env/mongo.env
    ports:
      - '27017:27017'
volumes:
  data:
EOF

cat << EOF >> ./docker/env/mongo.env
MONGO_INITDB_ROOT_USERNAME=tomekuser
MONGO_INITDB_ROOT_PASSWORD=secretkey123
EOF

cd docker
docker-compose up