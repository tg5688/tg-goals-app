#!/bin/bash

sudo yum update -y

wget https://downloads.mongodb.com/compass/mongosh-1.0.3-linux-x64.tgz
tar -zxvf mongosh-1.0.3-linux-x64.tgz
chmod +x mongosh-1.0.3-linux-x64/bin/mongosh
chmod +x mongosh-1.0.3-linux-x64/bin/mongocryptd-mongosh
sudo cp mongosh-1.0.3-linux-x64/bin/mongosh /usr/local/bin/
sudo cp mongosh-1.0.3-linux-x64/bin/mongocryptd-mongosh /usr/local/bin/