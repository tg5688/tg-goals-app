resource "aws_network_interface" "bastion" {
  subnet_id       = aws_subnet.public_a.id
  security_groups = [aws_security_group.bastion.id]

  tags = {
    Name = "primary_network_interface"
  }
}
resource "aws_network_interface" "bastion-priv" {
  subnet_id       = aws_subnet.private_a.id
  security_groups = [aws_security_group.bastion.id]

  private_ips = ["10.1.10.200"]
  tags = {
    Name = "second_network_interface"
  }
}
resource "aws_instance" "bastion" {
  ami           = data.aws_ami.amazon_linux.id
  instance_type = "t2.micro"
  # subnet_id     = aws_subnet.public_a.id
  user_data = file("./templates/ec2/user-data-bastion.sh")
  key_name  = var.keyname
  # private_ip    = "10.1.10.250"

  network_interface {
    network_interface_id = aws_network_interface.bastion.id
    device_index         = 0
  }

  network_interface {
    network_interface_id = aws_network_interface.bastion-priv.id
    device_index         = 1
  }

  # vpc_security_group_ids = [
  #   aws_security_group.bastion.id
  # ]

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${var.project}-bastion" })
  )
}

resource "aws_security_group" "bastion" {
  description = "Control bastion inbound and outbound access"
  name        = "${var.project}-bastion"
  vpc_id      = aws_vpc.main.id

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["89.25.216.1/32", "87.205.0.0/16"]
  }

  egress {
    protocol  = "tcp"
    from_port = 22
    to_port   = 22
    cidr_blocks = [
      aws_subnet.private_a.cidr_block,
    aws_subnet.private_b.cidr_block]
  }

  egress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 27017
    to_port   = 27017
    protocol  = "tcp"
    cidr_blocks = [
      aws_subnet.private_a.cidr_block,
    aws_subnet.private_b.cidr_block]
  }

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${var.project}-bastion" })
  )
}
