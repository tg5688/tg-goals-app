terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
  backend "s3" {
    bucket  = "tg-goals-app-api-devops-tfstate"
    key     = "goals-app.tfstate"
    region  = "eu-west-1"
    encrypt = true
  }
}


provider "aws" {
  region = var.region
}

locals {
  common_tags = {
    Project   = var.project
    Owner     = "TomekGunter"
    ManagedBy = "Terraform"
  }
}

data "aws_region" "current" {}
